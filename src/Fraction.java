/**
 * Created by przemek on 27.06.17.
 */
public class Fraction {
    private final long numerator;
    private final long denominator;

    public Fraction(long numerator, long denominator) {
        if (denominator == 0) {
            throw new IllegalArgumentException("Denominator is zero.");
        }
        if (denominator <0){
            denominator = (-1) * denominator;
            numerator = (-1) * numerator;
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction add(Fraction fraction) {
        long nww = this.nww(fraction, this);
        return new Fraction(
            (nww / this.getDenominator()) * this.getNumerator() + (nww / fraction.getDenominator()) * fraction.getNumerator(),
            nww
        );
    }

    public Fraction substract(Fraction fraction) {
        long nww = this.nww(fraction, this);
        return new Fraction(
            (nww / this.getDenominator()) * this.getNumerator() - (nww / fraction.getDenominator()) * fraction.getNumerator(),
            nww
        );
    }

    public Fraction multiple(Fraction multiplicand) {
        return new Fraction(this.getNumerator() * multiplicand.getNumerator(), this.getDenominator() * multiplicand.getDenominator());
    }

    public Fraction divide(Fraction dividerParam) {
        Fraction divider = dividerParam.inverse();
        return this.multiple(divider);
    }

    public Fraction inverse() {
        return new Fraction(this.getDenominator(), this.getNumerator());
    }

    public int getIntegerPart() {
        return (int)(this.getNumerator() / this.getDenominator());
    }

    public long getNumerator() {
        return this.numerator;
    }

    public long getDenominator() {
        return this.denominator;
    }

    protected long nww(Fraction f1, Fraction f2) {
        long a = f1.getDenominator();
        long b = f2.getDenominator();
        long c = a;
        while (a % b != 0) {
            a += c;
        }
        return a;
    }
}