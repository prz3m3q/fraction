/**
 * Created by przemek on 02.07.17.
 */
public class FractionTest {
    public static void main(String[] arg){
        Fraction fraction1 = new Fraction(-1, 5);
        Fraction fraction2 = new Fraction(3, 7);
        Fraction fraction3 = fraction1.add(fraction2);
        Fraction fraction4 = fraction1.substract(fraction2);
        Fraction fraction5 = fraction1.multiple(fraction2);
        Fraction fraction6 = fraction1.divide(fraction2);
        System.out.println(fraction1.getNumerator() + " / " + fraction1.getDenominator());
        System.out.println(fraction2.getNumerator() + " / " + fraction2.getDenominator());
        System.out.println(fraction3.getNumerator() + " / " + fraction3.getDenominator());
        System.out.println(fraction4.getNumerator() + " / " + fraction4.getDenominator());
        System.out.println(fraction5.getNumerator() + " / " + fraction5.getDenominator());
        System.out.println(fraction6.getNumerator() + " / " + fraction6.getDenominator());
    }
}
